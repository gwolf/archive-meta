---
conference:
  title: 'MiniDebConf Online #2 "Gaming Edition"'
  series: MiniDebConf Online
  edition: 2
  location: Online
  date:
  - 2020-11-21
  - 2020-11-22
  website: https://mdco2.mini.debconf.org/
  schedule: https://mdco2.mini.debconf.org/schedule/
  video_base: https://meetings-archive.debian.net/pub/debian-meetings/2020/MiniDebConfOnline2-Gaming/
  video_formats:
    default:
      resolution: 1280x720
      bitrate: 1024k
      vcodec: vp9
      acodec: opus
    lq:
      resolution: 640x360
      bitrate: 276k
      vcodec: vp8
      acodec: vorbis
videos:
- title: Opening session
  speakers:
  - Jonathan Carter
  - Stefano Rivera
  - Paul Waring
  - Nattie Mayer-Hutchings
  - Phil Morrell
  - Kyle Robbertze
  description: Let's get this party, er, conf started!
  details_url: https://mdco2.mini.debconf.org/talks/26-opening-session/
  room: Main
  start: 2020-11-21 11:45:00
  end: 2020-11-21 11:55:00
  video: opening-session.webm
  alt_formats:
    lq: opening-session.lq.webm
- title: ./play.it, a packages generator for DRM-free games
  speakers:
  - Antoine Le Gonidec
  - Mopi
  description: '[./play.it](https://wiki.debian.org/Games/PlayIt) is a libre software
    that automates the build of native packages for multiple distributions from [DRM-free](https://en.wikipedia.org/wiki/Digital_rights_management)
    installers for commercial games. The generated packages are then installed using
    the standard tools provided by the distribution.


    The purpose of this talk is to highlight this software that joined the Debian
    repositories with Debian Buster, providing a [KISS](https://en.wikipedia.org/wiki/KISS_principle)
    tool to DRM-free game players who do not want to use one of the numerous all-in-one
    game clients. We probably will talk about DRM quite a bit too ;)


    Our main target demographics are players, but this tool could be used by game
    developers too as a way to quickly generate packages for their games. [Contributions
    to ./play.it](https://forge.dotslashplay.it/play.it) are usually done by the players
    themselves, thanks to a syntax easy to grasp with no programming background.'
  details_url: https://mdco2.mini.debconf.org/talks/13-playit-a-packages-generator-for-drm-free-games/
  room: Main
  start: 2020-11-21 12:00:00
  end: 2020-11-21 12:20:00
  video: play-it-a-packages-generator-for-drm-fre.webm
  alt_formats:
    lq: play-it-a-packages-generator-for-drm-fre.lq.webm
- title: YIRL engine presentation
  speakers:
  - Matthias Gatto
  description: 'Short Presentation of my Game Engine YIRL: https://github.com/cosmo-ray/yirl.
    It''s a small modular 2D Game engine in C on which I work on my spare time. The
    Engine allow to create Game in C/Lua/Scheme/Javascript, which can target Linux
    and Windows. It also emulate partially ncurses and 8086 asm, and Javascript canvas
    API.'
  details_url: https://mdco2.mini.debconf.org/talks/3-yirl-engine-presentation/
  room: Main
  start: 2020-11-21 12:30:00
  end: 2020-11-21 12:50:00
  video: yirl-engine-presentation.webm
  alt_formats:
    lq: yirl-engine-presentation.lq.webm
- title: NetHack
  speakers:
  - Reiner Herrmann
  description: 'In this talk I want to give an overview over the roguelike game NetHack.

    Even though it is over 30 years old, it is still actively being developed and
    has an active community of players, with yearly tournaments and a bunch of friendly
    forks / variants.

    I will show how to get started, what interfaces there are and where to find additional
    information about this quite complex game.'
  details_url: https://mdco2.mini.debconf.org/talks/22-nethack/
  room: Main
  start: 2020-11-21 13:00:00
  end: 2020-11-21 13:20:00
  video: nethack.webm
  alt_formats:
    lq: nethack.lq.webm
- title: Overview of Gaming utilities in Debian
  speakers:
  - Stephan Lachnit
  description: In this talk I want to give a short overview of several Linux gaming
    utilities, and their state inside and outside of Debian. The tools I will cover
    are Lutris, Piper, MangoHud, gamemode, vkBasalt, GOverlay, vkdevicechooser, OpenRGB,
    Oversteer, GameHub and possibly more.
  details_url: https://mdco2.mini.debconf.org/talks/1-overview-of-gaming-utilities-in-debian/
  room: Main
  start: 2020-11-21 13:30:00
  end: 2020-11-21 14:10:00
  video: overview-of-gaming-utilities-in-debian.webm
  alt_formats:
    lq: overview-of-gaming-utilities-in-debian.lq.webm
- title: Orama Interactive Projects Showcase
  speakers:
  - Emmanouil Papadeas
  description: 'We are Orama Interactive, an independent game and software developer
    team. We have a passion for meaningful videogames, and libre software. In this
    talk, we will talk a bit about our projects, which include various games and a
    pixel art editor. We will go into detail about who we are, why we are doing what
    we do, the stories and the future plans of our projects, and how they are currently
    standing.


    Specifically, we will talk about 3 of our games, Where hope lies, a VR puzzle
    game, Repairing, a pixel art platformer game and Τechnotribal, a game mixing the
    quiz and puzzle genres. We will also have a closer look to Pixelorama, our currently
    in active development free and open source pixel art sprite editor'
  details_url: https://mdco2.mini.debconf.org/talks/24-orama-interactive-projects-showcase/
  room: Main
  start: 2020-11-21 14:30:00
  end: 2020-11-21 15:10:00
  video: orama-interactive-projects-showcase.webm
  alt_formats:
    lq: orama-interactive-projects-showcase.lq.webm
- title: Debian Games Team (BoF)
  speakers:
  - Jonathan Carter
  - Phil Morrell
  description: 'Interested in joining the Debian Games Team?

    Want to meet the Debian Games Team?

    What''s the current issues in the Debian Games Team that needs solving?


    Join us in this session to find out!'
  details_url: https://mdco2.mini.debconf.org/talks/18-debian-games-team-bof/
  room: Main
  start: 2020-11-21 15:30:00
  end: 2020-11-21 16:20:00
  video: debian-games-team-bof.webm
  alt_formats:
    lq: debian-games-team-bof.lq.webm
- title: Major game releases since buster
  speakers:
  - Phil Morrell
  description: Highlighting lots of the great work that has happened in the last 18
    months and now available in bullseye.
  details_url: https://mdco2.mini.debconf.org/talks/20-major-game-releases-since-buster/
  room: Main
  start: 2020-11-21 17:30:00
  end: 2020-11-21 17:50:00
  video: major-game-releases-since-buster.webm
  alt_formats:
    lq: major-game-releases-since-buster.lq.webm
- title: 0 A.D. Empires Ascendant, a FLOSS Game
  speakers:
  - Stanislas DOLCINI
  description: "This talk will cover the basics about 0 A.D: Empires Ascendant, the\
    \ game itself, the number of users, the team behind, the biggest mods, then we'll\
    \ dive more into the things we use under the hood, from C++ libraries to the contributing\
    \ platform and CI, hosting. \n\nThen I'll answer questions during the remaining\
    \ time, and I can go more in depth about specific topics."
  details_url: https://mdco2.mini.debconf.org/talks/16-0-ad-empires-ascendant-a-floss-game/
  room: Main
  start: 2020-11-21 18:30:00
  end: 2020-11-21 19:10:00
  video: 0-a-d-empires-ascendant-a-floss-game.webm
  alt_formats:
    lq: 0-a-d-empires-ascendant-a-floss-game.lq.webm
- title: FOSS Virtual & Augmented Reality
  speakers:
  - Tim Jakob Bornecrantz
  description: 'In this talk will cover Monado and Khronos'' OpenXR standard, and
    give an overview about the current state of open source VR and what lies ahead.
    Also go into some details of how tracking is done inside of Monado and show of
    the current state.


    VR took off for the consumer with the release of Oculus consumer hardware. But
    the hardware lacked open source drivers and Linux support in general. The consumer
    VR space has now grown from a kickstarter campaign into a large industry. But
    this growth has its down sides, multiple companies have their own APIs competing.
    Luckily these companies have agreed to work on a single API under the Khronos
    umbrella. Now that OpenXR has been released and and the Monado project has been
    getting more stable it is now possible to do good VR on a completely open stack.'
  details_url: https://mdco2.mini.debconf.org/talks/2-foss-virtual-augmented-reality/
  room: Main
  start: 2020-11-21 19:30:00
  end: 2020-11-21 20:10:00
  video: foss-virtual-augmented-reality.webm
  alt_formats:
    lq: foss-virtual-augmented-reality.lq.webm
- title: Open Source Game Achievements
  speakers:
  - Dennis Payne
  description: 'Players now expect to record their game achievement to show their
    friends. Usually this is implemented differently for each game store. This requires
    developers to code the solution multiple times and create multiple builds. They
    won''t support games acquired outside of the game store like those included in
    Debian. The game stores controls your data not you. Gamerzilla can help solve
    this problem.


    Gamerzilla is an open source achievements for games. Development started after
    LibrePlanet 2020. It consists of multiple pieces. Games use the LibGamerzilla
    library to update the progress on achievements. Game launchers use the same library
    to keep track of achievements and to upload them. The Gamerzilla plugin for Hubzilla
    displays the achievements online but the protocol is simple enough it could be
    implemented in something else or a standalone service.


    LibGamerzilla is already included in Fedora. Support is being added to several
    games.'
  details_url: https://mdco2.mini.debconf.org/talks/5-open-source-game-achievements/
  room: Main
  start: 2020-11-21 20:30:00
  end: 2020-11-21 20:50:00
  video: open-source-game-achievements.webm
  alt_formats:
    lq: open-source-game-achievements.lq.webm
- title: 'GemRB: 20 years of the engine and a look forward'
  speakers:
  - Jaka Kranjc
  description: 'A brief look at the RPG [engine''s](https://gemrb.org "engine''s")
    history & future, capabilities, potential for new games and the seemingly complicated
    relationship with game authors.


    This light-hearted talk won''t be very technical and targets both developers and
    gamers alike.'
  details_url: https://mdco2.mini.debconf.org/talks/7-gemrb-20-years-of-the-engine-and-a-look-forward/
  room: Main
  start: 2020-11-22 12:00:00
  end: 2020-11-22 12:20:00
  video: gemrb-20-years-of-the-engine-and-a-look-.webm
  alt_formats:
    lq: gemrb-20-years-of-the-engine-and-a-look-.lq.webm
- title: 'Lesverhaal: a visual novel engine for use in a classroom setting'
  speakers:
  - Bas Wijnen
  description: 'Watching people discus a subject is a good way to learn about it.
    With this in mind, I decided that a visual novel would be a good way to get middle
    and high school students to learn about physics in a pleasant way. Instead of
    (or in addition to) answering questions about what they will do next, they can
    answer questions about the physics, to demonstrate that they understood it.


    For writing visual novels, Ren''Py is the engine of choice. However, it has some
    drawbacks for this purpose. Most notably that as a teacher I want to monitor their
    progress. So I want to see their answers. That is not (reasonably) possible with
    Ren''Py. So I decided to write my own engine, which for now goes by the name "Lesverhaal".


    In this talk, I''ll explain the design of Lesverhaal, how I solved some problems
    and what problems are still left to solve. Spoiler: the main thing left to create
    is not code, but story content.'
  details_url: https://mdco2.mini.debconf.org/talks/15-lesverhaal-a-visual-novel-engine-for-use-in-a-classroom-setting/
  room: Main
  start: 2020-11-22 12:30:00
  end: 2020-11-22 12:50:00
  video: lesverhaal-a-visual-novel-engine-for-use.webm
  alt_formats:
    lq: lesverhaal-a-visual-novel-engine-for-use.lq.webm
- title: Lightning Talks
  speakers:
  - Nattie Mayer-Hutchings
  description: Here's your chance to talk very briefly on a subject related to games
    on Linux.  Talks can be up to 5 minutes in length, and can be submitted prerecorded
    (preferred) or done live if necessary.  Please contact Team Lightning on <islightningreal@debconf.org>
    to register your interest.
  details_url: https://mdco2.mini.debconf.org/talks/19-lightning-talks/
  room: Main
  start: 2020-11-22 13:00:00
  end: 2020-11-22 13:20:00
  video: lightning-talks.webm
  alt_formats:
    lq: lightning-talks.lq.webm
- title: Castle Game Engine - overview and upcoming features
  speakers:
  - Michalis Kamburelis
  description: '[Castle Game Engine](https://castle-engine.io/) is a cross-platform
    open-source game engine using modern Object Pascal. It runs perfectly on Linux
    (and was mostly made on Linux), as well as other platforms (Windows, macOS, Android,
    iOS, Nintendo Switch). We use FPC and Lazarus (Pascal compiler and IDE, packaged
    in Debian as well). We proud ourselves on having support for many formats (including
    open standards glTF and X3D), visual editor and lots of graphic features.


    During the talk I want to give a short overview how creating a simple game using
    Castle Game Engine looks like. We''ll start from scratch, use the engine to create
    a new project, use Blender to design some amazing 3D programmer-art :), and put
    it into a small demo game. I will showcase and talk about Castle Game Engine new
    (soon-to-be-released) version 7.0.'
  details_url: https://mdco2.mini.debconf.org/talks/21-castle-game-engine-overview-and-upcoming-features/
  room: Main
  start: 2020-11-22 13:30:00
  end: 2020-11-22 14:10:00
  video: castle-game-engine-overview-and-upcoming.webm
  alt_formats:
    lq: castle-game-engine-overview-and-upcoming.lq.webm
- title: My DiY Pinball on Debian
  speakers:
  - Philippe Coval
  description: "Debian gamers might already know \"Emilia Pinball\" which is in main\
    \ since 2007 \nand did not evolve much since and package was orphaned.\n\nBut\
    \ this does not stop here !\n\nDuring 2020 lock-down (first one) to get busy,\
    \ I started to build my \"Pincab\" machine\nto push the experience forward.\n\n\
    I'll explain the journey of this hobby project and how to replicate easily home\
    \ \non your favorite Debian or Yocto system using low cost materials.\n\nExpect\
    \ demos and track progress at:\n\nhttps://purl.org/rzr/pinball\n\nMeanwhile you\
    \ can play with extra community tables I published in main repos."
  details_url: https://mdco2.mini.debconf.org/talks/23-my-diy-pinball-on-debian/
  room: Main
  start: 2020-11-22 14:30:00
  end: 2020-11-22 14:50:00
  video: my-diy-pinball-on-debian.webm
  alt_formats:
    lq: my-diy-pinball-on-debian.lq.webm
- title: Chiaki - bringing PlayStation 4 Remote Play to Linux
  speakers:
  - Florian Märkl
  description: 'Chiaki is an open source implementation of Sony''s PlayStation 4 Remote
    Play, which is a game streaming protocol that allows the PlayStation 4 to be played
    remotely over the network. The talk will contain a brief overview of the protocol
    and the reverse engineering process that was used to obtain all of this information,
    as well as a few short demos of Chiaki in action.

    It is intended for anyone interested in the technical details of low-latency game
    streaming or reverse engineering closed source applications.'
  details_url: https://mdco2.mini.debconf.org/talks/9-chiaki-bringing-playstation-4-remote-play-to-linux/
  room: Main
  start: 2020-11-22 15:00:00
  end: 2020-11-22 15:40:00
  video: chiaki-bringing-playstation-4-remote-pla.webm
  alt_formats:
    lq: chiaki-bringing-playstation-4-remote-pla.lq.webm
- title: 'Notcurses: Making terminals do things that were never intended'
  speakers:
  - nick black
  description: 'Notcurses 2 has recently (2020-10) been released, providing for the
    first time a stable, guaranteed API for this blingful character graphics library.
    Notcurses, sometimes described as "NCURSES and FFmpeg had a baby", is a TUI library
    with unique capabilities for multimedia, including streaming video support, the
    advanced Quadblitter, palette fades, and plane rotations. It is thus not unreasonable
    that developers might want to use it for terminal games (assuming that terminal
    games are a reasonable thing in the first place).


    This short talk will cover the shortcomings of the X/Open Curses API and NCURSES
    (its most common instantiation), effective use of Notcurses and its advanced features,
    and a brief practicum involving the live coding of a terminal game.


    BLING BLING.'
  details_url: https://mdco2.mini.debconf.org/talks/6-notcurses-making-terminals-do-things-that-were-never-intended/
  room: Main
  start: 2020-11-22 17:30:00
  end: 2020-11-22 18:10:00
  video: notcurses-making-terminals-do-things-tha.webm
  alt_formats:
    lq: notcurses-making-terminals-do-things-tha.lq.webm
- title: 'Community Game Development in Rust: A Biopsy'
  speakers:
  - Forest Anderson
  description: 'Veloren is an open-world, open-source multiplayer voxel RPG written
    completely in Rustlang. It has similar aesthetics to Minecraft, and is inspired
    by other games like Cube World and Dwarf Fortress. The project was started in
    the summer of 2018, and has since seen over 100 unique contributors. Veloren has
    first-class Linux support, and also targets Mac and Windows. A majority of the
    developers also work from Linux-based systems.


    This talk will be geared towards open-source game developers, developers interested
    in Rust, and those interested in complex engine systems. It will focus on many
    lessons that have been learned from the Veloren project. Some main points include
    building a diverse community, why Rust works so well for the project, and interesting
    concepts from engine development. The talk will also highlight some interesting
    optimizations we''ve found, and how we diagnosed them.


    Veloren has contributors from all around the world, and has gone through many
    organizational transitions to build a healthy community. Novel and cutting-edge
    research has been implemented in Veloren. Many people from different backgrounds
    have added their flair to Veloren, creating a unique visual experience. This talk
    will examine how it was all built.'
  details_url: https://mdco2.mini.debconf.org/talks/8-community-game-development-in-rust-a-biopsy/
  room: Main
  start: 2020-11-22 18:30:00
  end: 2020-11-22 19:10:00
  video: community-game-development-in-rust-a-bio.webm
  alt_formats:
    lq: community-game-development-in-rust-a-bio.lq.webm
- title: 'Building an ecosystem as a service: how to stop suffering from “that code
    is meant to be forked”'
  speakers:
  - Thomas Debesse
  description: Earlier idTech engines were well known to have seen their source code
    opened when they were replaced and thus unprofitable. While this was a huge benefit
    for the mankind, game developers still suffer today with design choices and mindset
    induced from the fact such code base was meant to die. 20 years later we will
    focus on idTech3 heritage, how both the market, open source communities and game
    development practices evolved, and embark in the journey of the required transition
    from dead code dump to an ecosystem as a service.
  details_url: https://mdco2.mini.debconf.org/talks/25-building-a-community-as-a-service-how-to-stop-suffering-from-that-code-is-meant-to-be-forked/
  room: Main
  start: 2020-11-22 19:30:00
  end: 2020-11-22 20:10:00
  video: building-an-ecosystem-as-a-service-how-t.webm
  alt_formats:
    lq: building-an-ecosystem-as-a-service-how-t.lq.webm
- title: Music assets in Videogames and free tools.
  speakers:
  - Francesco Corrado
  description: '**Introduction**

    Presentation and talk about general state of linux gaming and the tools we have
    to make Libre games and Opensource games.


    **The talk**

    The talk will be about assets in game and specifically about music and sounds
    in games done with free tools. Sometimes to achieve good quality production you
    need to use other non free tools too.


    It''s important to know which tools we have and how to improve them offering an
    alternative to commercial well known tools out there.'
  details_url: https://mdco2.mini.debconf.org/talks/14-music-assets-in-videogames-and-free-tools/
  room: Main
  start: 2020-11-22 20:30:00
  end: 2020-11-22 20:50:00
  video: music-assets-in-videogames-and-free-tool.webm
  alt_formats:
    lq: music-assets-in-videogames-and-free-tool.lq.webm
- title: Closing session
  speakers:
  - Jonathan Carter
  description: Bye, everyone, see you next time!
  details_url: https://mdco2.mini.debconf.org/talks/27-closing-session/
  room: Main
  start: 2020-11-22 21:00:00
  end: 2020-11-22 21:20:00
  video: closing-session.webm
  alt_formats:
    lq: closing-session.lq.webm
