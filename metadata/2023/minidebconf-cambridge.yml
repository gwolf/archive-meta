---
conference:
  title: MiniDebConf Cambridge 2023
  series: MiniDebConf Cambridge
  location: Cambridge, United Kingdom
  date:
  - 2023-11-23
  - 2023-11-26
  website: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge
  schedule: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge#Schedule
  video_base: https://meetings-archive.debian.net/pub/debian-meetings/2023/MiniDebConf-Cambridge/
  video_formats:
    default:
      resolution: 1280x720
      bitrate: 1024k
      vcodec: vp9
      acodec: opus
    lq:
      resolution: 640x360
      bitrate: 280k
      vcodec: vp9
      acodec: opus
videos:
- title: Welcome to Cambridge!
  speakers:
  - Steve McIntyre
  description: Welcome talk and housekeeping
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Welcome
  room: main
  video: welcome-to-cambridge.webm
  alt_formats:
    lq: welcome-to-cambridge.lq.webm
  slides:
  - slides/Welcome.pdf
- title: PAC and BTI in Debian, what are they and why should I care?
  speakers:
  - Steve Capper
  description: We have recently enabled Pointer Authentication (PAC) and Branch
    Target Identifiers (BTI) in Debian Sid. These are two Arm architectural
    features that aim to improve security. In this talk I intend to introduce
    PAC and BTI architecturally, explain how they are used by software, show how
    we've enabled them in Debian, and give some hints on how to debug issues caught
    by PAC and BTI (should they arise!).
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Capper
  room: main
  video: pac-and-bti-in-debian.webm
  alt_formats:
    lq: pac-and-bti-in-debian.lq.webm
  slides:
  - slides/miniconf-2023-PAC-and-BTI.pdf
- title: "Hardware enablement in Debian: the Thinkpad X13s experience"
  speakers:
  - Emanuele Rocca
  description: The talk will go through the steps needed to enable new hardware
    in Debian. In increasing order of difficulty, we will see how to enable new
    kernel modules, modify the Installer, build custom Live Images and CDs.
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Rocca
  room: main
  video: hardware-enablement-in-debian.webm
  alt_formats:
    lq: hardware-enablement-in-debian.lq.webm
  slides:
  - slides/debian-x13s.pdf
- title: Using Debian on mobile phones (BoF)
  speakers:
  - Arnaud Ferraris
  description: Over the past few years, readily-available Linux-first phones
    have allowed important developments the mobile Linux community. This also
    triggered an effort to improve Debian's support for those devices, within both
    Debian (through the DebianOnMobile team) and its close derivative Mobian.
    During this session, we will first go through the current state of mobile
    Debian, present the work of the DebianOnMobile team and expose the remaining
    pain points and problems we're facing. This short presentation will be followed
    by an open discussion regarding how we can better support mobile devices in
    Debian.
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Ferraris
  room: main
  video: using-debian-on-mobile-phones-bof.webm
  alt_formats:
    lq: using-debian-on-mobile-phones-bof.lq.webm
  slides:
  - slides/DebianOnMobile-BoF.pdf
- title: Lightning talks (Saturday)
  speakers:
  - Helmut Grohne
  - Edlira Nano
  - Carles Pina i Estany
  description: Lightning talks
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/lightning
  room: main
  video: lightning-talks-sat.webm
  alt_formats:
    lq: lightning-talks-sat.lq.webm
- title: Adventures in bootloading - How the SteamOS bootloader works (and why it works that way)
  speakers:
  - Vivek Das Mohapatra
  description: How the SteamOS bootloader works (and why it works that way)
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Dasmohapatra
  room: main
  video: adventures-in-bootloading.webm
  alt_formats:
    lq: adventures-in-bootloading.lq.webm
  slides:
  - slides/bootloader.pdf
- title: CHERI and Morello - Arming systems with hardware-enforced memory safety capabilities
  speakers:
  - Jessica Clarke
  description: 1988 saw the Morris worm exploit a buffer overflow in fingerd,
    yet 35 years later unsafe C and C++ remain ubiquitous, with Microsoft reporting
    in 2019 that around 70% of their vulnerabilities are memory safety errors, and
    Google reporting the same for Chrome in 2020. CHERI is a research project at
    the University of Cambridge and SRI International, in collaboration with
    industry, that seeks to address such vulnerabilities at the hardware level, and
    Morello is a prototype architecture, SoC and development board developed by Arm
    that brings CHERI to a variant of the Armv8.2-A architecture. In this talk I’ll
    give an overview of how CHERI works, its interactions with C and C++, what
    software we have running on top of it, and a short demo.
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Clarke
  room: main
  video: cheri-and-morello.webm
  alt_formats:
    lq: cheri-and-morello.lq.webm
  slides:
  - slides/cheri-and-morello.pdf
- title: Long Term Support starts in unstable
  speakers:
  - Roberto C. Sánchez
  - Santiago Ruano Rincón
  description: While Debian LTS is in many respects a distinct effort/project
    separate from other aspects of the Debian project, it is also true that Long
    Term Support really begins in unstable. This talk will consider ways in which
    maintainers of packages can make package maintenance choices in unstable that
    will result in a better and smoother experience for those who use and maintain
    packages throughout the five years after the initial Debian release. This talk
    will also touch on ways in which the LTS maintenance team is prepared to assist
    maintainers of packages in unstable.
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Sanchez
  room: main
  video: long-term-support-starts-in-unstable.webm
  alt_formats:
    lq: long-term-support-starts-in-unstable.lq.webm
  slides:
  - slides/long-term-support-starts-in-unstable.odp
- title: tag2upload - uploads, made automatically and fully traceably, from git
  speakers:
  - Ian Jackson
  description: Uploading to Debian is a complicated and balky business
    involving tarballs, patches, and arcane runes. tag2upload is a scheme to help
    improve this - maintainers simply sign a suitable git tag, which is much more
    convenient for them, and a robot automatically, reliably, and traceably, builds
    the source package and uploads it to the Debian archive.
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Jackson
  room: main
  video: tag2upload.webm
  alt_formats:
    lq: tag2upload.lq.webm
  slides:
  - slides/tag2upload.pdf
- title: Updates
  speakers:
  - Steve McIntyre
  description: Start of Sunday
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Updates
  room: main
  video: updates.webm
  alt_formats:
    lq: updates.lq.webm
- title: Meet the Release Team
  speakers:
  - Paul Gevers
  description: In this session the Release Team will report on the ideas
    generated and the work done during the Cambridge sprint. The session also gives
    the audience an opportunity to ask questions related to the work of the Release
    Team.
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Gevers
  room: main
  video: meet-the-release-team.webm
  alt_formats:
    lq: meet-the-release-team.lq.webm
  slides:
  - slides/Cambridge2023_meet-the-release-team.odp
- title: Testing, A journey from Kernel testing to testing Debian unstable
  speakers:
  - Sudip Mukherjee
  description: Testing is an integral part of the software lifecycle. For
    software which are in continuous development it's even more important to have
    regular testing so that any bugs or errors can be detected early. In this talk,
    I will present how I started testing the Linux Kernel in a personal capacity
    and the status of Kernel testing that is now being done as part of Codethink. I
    will also present how that testing infrastructure has evolved to test Debian
    Sid on a RPI4 from a CI pipeline and the problems we had to overcome.
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Mukherjee
  room: main
  video: testing.webm
  alt_formats:
    lq: testing.lq.webm
  slides:
  - slides/testing_minidebconf.pdf
- title: Meet the Community Team
  speakers:
  - Andrew M A Cater
  description: We are the Debian Community Team. Amongst our weapons are
    secrecy, skulking in corners, humiliating developers and being difficult to all
    comers - or that's what some people would have you believe.  We're not quite
    like that - here's an explanation of who we are and what we do.
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Cater
  room: main
  video: meet-the-community-team.webm
  alt_formats:
    lq: meet-the-community-team.lq.webm
  slides:
  - slides/CommunityTeam2023.pdf
- title: Lightning talks (Sunday, first session)
  speakers:
  - Colin Watson
  - Héctor Orón Martínez
  - Francis Murtagh
  - Andy Cater
  - Chris Walker
  description: Lightning talks
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/lightning
  room: main
  video: lightning-talks-sun-1.webm
  alt_formats:
    lq: lightning-talks-sun-1.lq.webm
- title: Supporting complex camera stacks
  speakers:
  - Ricardo Ribalda
  description: Cameras have become an integral part of the consumer products,
    and as a result of that their specs keep growing every year. With new
    features comes new/experimental software stacks. In this session we would like
    to discuss how to support these new camera stacks during their trip to
    upstream, and what is the position of distros in regards to supporting new
    hardware.
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Ribalda
  room: main
  video: supporting-complex-camera-stacks.webm
  alt_formats:
    lq: supporting-complex-camera-stacks.lq.webm
  slides:
  - slides/supporting-complex-camera-stacks.pdf
- title: Lightning talks (Sunday, second session)
  speakers:
  - David Heidelberg
  - Ian Jackson
  description: Lightning talks
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/lightning
  room: main
  video: lightning-talks-sun-2.webm
  alt_formats:
    lq: lightning-talks-sun-2.lq.webm
- title: QKD and Quantum-resistant Crypto
  speakers:
  - Robert Woodward
  - Andy Simpkins
  description: We'll introduce Quantum Key Distribution (QKD) a technique for
    providing secure key delivery between nodes and take a quick look at Digital
    Security and Authentication in a world with Quantum Computers.
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Simpkins
  room: main
  video: qkd-and-quantum-resistant-crypto.webm
  alt_formats:
    lq: qkd-and-quantum-resistant-crypto.lq.webm
  slides:
  - slides/qkd-and-quantum-resistant-crypto.pdf
- title: 2038 update
  speakers:
  - Wookey
  description: The wrap of 32-bit time_t in Jan 2038 is only 15 years away. The
    people designing 32-bit linux products/systems still likely to be in use then
    have long product cycles so fixing this has become quite urgent. The base work
    has been done, but how to manage this ABI transition in debian needed some
    research, and planning.  We now have a reasonable idea of the size of the task,
    and this talk will remind you of the basics and give an update on what we have
    found and where we are at.
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Wookey
  room: main
  video: y2038-update.webm
  alt_formats:
    lq: y2038-update.lq.webm
  slides:
  - slides/yr2038-minidebconf.pdf
- title: Introducing Debusine
  speakers:
  - Enrico Zini
  - Carles Pina i Estany
  description: Debusine manages scheduling and distribution of Debian-related
    tasks (package build, lintian analysis, autopkgtest runs, etc.) to distributed
    worker machines. It is being developed by Freexian with the intention of giving
    people access to a range of pre-configured tools and workflows running on
    remote hardware. Freexian obtained STF funding for a substantial set of
    Debusine milestones, so development is happening on a clear schedule. We can
    present where we are and, we're going to be, and what we hope to bring to
    Debian with this work.
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Zini
  room: main
  video: debusine.webm
  alt_formats:
    lq: debusine.lq.webm
  slides:
  - slides/slides.pdf
- title: Close
  speakers:
  - Steve McIntyre
  description: It's all over...
  details_url: https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge/Close-2
  room: main
  video: close-2.webm
  alt_formats:
    lq: close-2.lq.webm
  slides:
  - slides/Sunday-summary.pdf
