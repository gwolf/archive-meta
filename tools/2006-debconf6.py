#!/usr/bin/env python3

import dateutil.parser
import requests
from bs4 import BeautifulSoup

from utils.objects import Event
from utils.yaml import yaml_dump


def scrape():
    r = requests.get('https://video.debian.net/2006/debconf6/')

    soup = BeautifulSoup(r.content, 'html.parser')
    table = soup.find_all('table')[2]
    for row in table.find_all('tr'):
        event = Event(speakers=[], slides=[], alt_formats={})
        cells = list(enumerate(row.find_all('td')))
        if len(cells) == 1:
            continue
        for column, cell in cells:
            if column == 0:
                event.start = dateutil.parser.parse(cell.get_text()).date()
            if column == 1:
                event.title = cell.get_text()
            if column == 2:
                event.speakers = [
                    name.strip() for name in cell.get_text().split(';')]
            if column == 3:
                for link in cell.find_all('a'):
                    href = link['href']
                    format_ = link.get_text()
                    if format_ == 'theora-unscaled':
                        event.video = href
                    else:
                        event.alt_formats[format_] = href
        yield event


def main():
    events = list(scrape())

    with open('scraped.yml', 'w') as f:
        yaml_dump(events, f)


if __name__ == '__main__':
    main()
