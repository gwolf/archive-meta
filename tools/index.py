#!/usr/bin/env python3

from datetime import date
from pathlib import Path

from utils.yaml import yaml_dump, load_meta


def main():
    metadata = Path('metadata')
    index = metadata / 'index.yml'

    files = [path for path in Path('metadata').glob('**/*.yml')
             if path != index]
    files.sort(key=sort_key)

    rel_files = [
        str(path.relative_to(metadata))
        for path in files]

    with index.open('w') as f:
        yaml_dump({'files': rel_files}, f)


def sort_key(path):
    """Return the conference's (start date, filename) for sorting"""
    with path.open() as f:
        data = load_meta(f)
        conference = data.conference
        try:
            start = conference.date[0]
        except AttributeError:
            print('No Date:', path)
            start = date(int(path.parent.name), 12, 31)
    return (start, str(path))


if __name__ == '__main__':
    main()
