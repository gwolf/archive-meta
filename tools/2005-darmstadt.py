#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup

from utils.objects import Event
from utils.yaml import yaml_dump


def scrape():
    r = requests.get('https://video.debian.net/2005/qa-meeting-darmstadt/')

    soup = BeautifulSoup(r.content, 'html.parser')
    for p in soup.find_all('p'):
        if p.string and p.string.startswith('2005'):
            continue
        description = p.get_text()
        description = description.rsplit(':', 1)[0]
        if description.startswith('The meeting'):
            break
        description, _, speakers = description.rpartition(' by ')
        speakers = [
            speaker.strip()
            for speaker in speakers.split(',')]
        event = Event(
            title=description, speakers=speakers,
            slides=[], alt_formats={}, language='eng')
        for link in p.find_all('a'):
            href = link['href'][2:]
            href = href.replace('/./', '/')
            if href.startswith('slides'):
                event.slides.append(href)
            elif href.startswith('good-quality/ogg'):
                event.video = href
            else:
                format_ = None
                if href.startswith('good-quality/mpeg'):
                    format_ = 'mpeg-high'
                if href.startswith('low-quality/ogg'):
                    format_ = 'ogg-low'
                if href.startswith('low-quality/mpeg'):
                    format_ = 'mpeg-low'
                if format_:
                    event.alt_formats[format_] = href
                else:
                    print('Ignoring href:', href)
        if not event.slides:
            del event.slides
        yield event


def main():
    events = list(scrape())

    with open('scraped.yml', 'w') as f:
        yaml_dump(events, f)


if __name__ == '__main__':
    main()
