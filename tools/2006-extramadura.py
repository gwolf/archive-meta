#!/usr/bin/env python3

import re

import dateutil.parser
import requests
from bs4 import BeautifulSoup

from utils.objects import Event
from utils.yaml import yaml_dump

DESC_RE = re.compile(r'^(?P<title>.*?)(?: \((?P<speakers>[^()]+)\))?$')


def scrape():
    r = requests.get('https://video.debian.net/2006/i18n-extremadura/')

    soup = BeautifulSoup(r.content, 'lxml')
    for li in soup.find_all('li'):
        description = li.get_text().split('\n')[0]
        m = DESC_RE.match(description)

        event = Event(
            title=m.group('title'),
        )
        speakers = m.group('speakers')
        if speakers:
            speakers = [speaker.strip() for speaker in speakers.split(',')]
            event.speakers = speakers

        for link in li.find_all('a'):
            href = link['href'][2:]
            event.start = dateutil.parser.parse(href.split('/')[0])
            event.video = href
        yield event


def main():
    events = list(scrape())

    with open('scraped.yml', 'w') as f:
        yaml_dump(events, f)


if __name__ == '__main__':
    main()
