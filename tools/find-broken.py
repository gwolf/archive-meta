#!/usr/bin/env python3
import argparse

from utils.files import find_used, known_files


def main():
    parser = argparse.ArgumentParser(
        description="Find files that aren't known to be in the archive")
    parser.parse_args()

    known = set(known_files())

    for video in find_used():
        if video not in known:
            print(video)


if __name__ == '__main__':
    main()
