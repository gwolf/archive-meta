import yaml

from utils.objects import Conference, Event, Meta, VideoFormat


def represent_item(dumper, data):
    nodes = []
    no_value = object()
    for key in data.keys:
        value = getattr(data, key, no_value)
        if value is not no_value:
            nodes.append((dumper.represent_data(key),
                          dumper.represent_data(value)))
    node = yaml.nodes.MappingNode('tag:yaml.org,2002:map', nodes)
    return node


def yaml_dump(data, file_object):
    """Opinionated yaml.dump"""
    file_object.write('---\n')
    yaml.safe_dump(
        data, file_object,
        allow_unicode=True, default_flow_style=False)


def load_meta(file_object):
    """Load meta into a Meta object"""
    data = yaml.safe_load(file_object)
    return load_meta_from_dict(data)


def load_meta_from_dict(data):
    formats = {name: VideoFormat(**format_)
               for name, format_
               in data['conference'].pop('video_formats', {}).items()}

    meta = Meta(
        conference=Conference(**data['conference']),
        videos=[Event(**video) for video in data['videos']],
    )
    if formats:
        meta.conference.video_formats = formats
    return meta


yaml.SafeDumper.add_representer(Conference, represent_item)
yaml.SafeDumper.add_representer(Event, represent_item)
yaml.SafeDumper.add_representer(Meta, represent_item)
yaml.SafeDumper.add_representer(VideoFormat, represent_item)
