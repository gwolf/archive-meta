import csv
import json
from pathlib import Path

from yaml import safe_load


def known_files():
    """Generator, yielding all files from the archive"""
    with open('data/video.debian.net.list') as f:
        for line in f:
            yield line.rstrip('\n')


def files_with_prefix(prefix, suffix=None):
    """Generator, yielding all files from the archive that start with prefix"""
    for file_ in known_files():
        if file_.startswith(prefix):
            if suffix and not file_.endswith(suffix):
                continue
            yield file_


def find_used():
    """Generator, yielding all files tracked in our metadata"""
    for manifest in metadata_files():
        with manifest.open() as f:
            data = safe_load(f)
        conference = data['conference']
        prefix = ''
        if conference['video_base']:
            prefix = conference['video_base']
            if prefix.startswith('https://meetings-archive.debian.net'
                                 '/pub/debian-meetings/'):
                prefix = prefix[56:]
            else:
                print("UNKNOWN Prefix", prefix)
        videos = data['videos']
        for video in videos:
            yield prefix + video['video']
            for format_, file_ in video.get('alt_formats', {}).items():
                yield prefix + file_


def penta_filename(id_):
    """Look up the base file name for penta event id_, from our database
    dump.
    """
    id_ = str(id_)
    with open('data/penta-recordings.json') as f:
        data = json.load(f)
    if id_ in data:
        return data[id_]
    return None


def dc11_filename(title):
    """debconf11's event_recording_base_name contents seems to have been lost.
    benh had some of this data in CSV
    """
    with open('data/debconf11.csv') as f:
        reader = csv.DictReader(f)
        for line in reader:
            if line['title'] == title:
                url = Path(line['url'])
                return url.stem


def metadata_files(basedir=Path('metadata')):
    """Read the metadata file index"""
    with (basedir / 'index.yml').open() as f:
        data = safe_load(f)
    for file_ in data['files']:
        yield basedir / file_
