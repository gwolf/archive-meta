#!/usr/bin/env python

import json

from pg import DB


def main():
    db = DB(dbname='pentabarf')

    q = db.query('SELECT event_id, event_recording_base_name '
                 'FROM video_event_recording;')

    data = dict(q.getresult())

    with open('penta-recordings.json', 'w') as f:
        json.dump(data, f, indent=2)


if __name__ == '__main__':
    main()
