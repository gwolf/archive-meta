#!/usr/bin/env python3

import argparse
import json
from datetime import date
from pathlib import Path

import yaml

from utils.files import metadata_files


def json_serialize(obj):
    """Serialize obj to JSON"""
    if isinstance(obj, date):
        return obj.isoformat()
    raise TypeError(
        'Object of type {} is not JSON serializable'.format(type(obj)))


def main():
    p = argparse.ArgumentParser(
        'Build JSON equivalents of all YAML documents under ROOT')
    p.add_argument('root', type=Path,
                   help='Base directory to operate on')
    args = p.parse_args()

    files = []
    for path in metadata_files(args.root):
        json_path = path.with_suffix('.json')
        with path.open() as f:
            data = yaml.safe_load(f)
        with json_path.open('w') as f:
            json.dump(data, f, default=json_serialize)

        files.append(str(json_path.relative_to(args.root)))

    with (args.root / 'index.json').open('w') as f:
        json.dump({'files': files}, f)


if __name__ == '__main__':
    main()
